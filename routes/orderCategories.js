const router = require('express').Router();
const DbWorker = require('../services/db-worker');

router.route('/')
    .get(async function (req, res) {
        try {
            const orderCategories = await DbWorker.getAll();
            await res.status(200).json(orderCategories);
        } catch (err) {
            return res.status(500).send(err);
        }
    })

    .post(async function (req, res) {
        try {
            const newData = await DbWorker.create(req.body);
            await res.status(201).json(newData);
        } catch (err) {
            return res.status(500).send(err);
        }
    });

module.exports = router;
